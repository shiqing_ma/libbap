#!/usr/bin/env python
#
# stp.py
# Copyright (C) 2013 Carl Pulley <c.j.pulley@hud.ac.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details. 
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 

import tempfile

#---------------------------------------------------------------
# Theorem Prover Options
#---------------------------------------------------------------

def quiet():
  "Quiet: Supress outputting the WP in the BAP IL."
  return [["topredicate", "-q"]]

#---------------------------------------------------------------
# Theorem Prover Commands
#---------------------------------------------------------------

def pp_wp(filename=None):
  """
  Print weakest pre-condition (WP) expression (without STP details)

  <filename> if specified, then print to <filename>
  """
  if filename == None:
    return [["topredicate", "-pstp-out"]]
  else:
    return [["topredicate", "-pstp-out", filename]]

def post(expr="true"):
  "Use <expr> as the post-condition"
  return [["topredicate", "-post", expr]]

def simp_ssa(usesccvn=True, usedc=True, usecjmp=True):
  """
  Perform all specified optimizations on static-single assignment (SSA) graph

  <usesccvn>: strongly connected component based value numbering (SCCVN)
  <usedc>   : dead code elimination
  <usecjmp> : replace conditional jumps, that have a constant as their expression, by a jump statement
  """
  return [["iltrans", "-simp-ssa", str(usesccvn).lower(), str(usedc).lower(), str(usecjmp).lower()]]

def fse_bfs():
  """
  Use naive forward symbolic execution with breath first search.

  Produces exponentially sized formulas.
  """
  return [["topredicate", "-fse-bfs"]]

def fse_bfs_maxdepth(num):
  """
  FSE with breath first search, limiting search depth to <num>.

  Produces exponentially sized formulas.
  """
  return [["topredicate", "-fse-bfs-maxdepth", str(num)]]

def fse_maxrepeat(num):
  """
  FSE excluding walks that visit a point more than <num> times.

  Produces exponentially sized formulas.
  """
  return [["topredicate", "-fse-maxrepeat", str(num)]]

def solve(filename=None, usedc=True, usesccvn=True, timeout=0, solver="stp", validity=False):
  """
  Solve the generated (WP) formula 

  <filename>: if specified, save the STP formula to <filename>
  <usedc>   : dead code elimination
  <usesccvn>: strongly connected component based value numbering (sccvn)
  <solver>  : the prover to use (possible choices: stp; stp_smtlib; cvc3; cvc3_smtlib; yices; z3)
  <timeout> : set prover solving timeout in seconds
  <validity>: check for validity (if False, satisfiability is checked)
  """
  if usedc and usesccvn:
    config = [["topredicate", "-opt"]]
  elif usedc and not usesccvn:
    config = [["topredicate", "-optdc"]]
  elif not usedc and usesccvn:
    config = [["topredicate", "-optsccvn"]]
  else:
    config = [["topredicate", "-noopt"]]
  if solver in ["stp_smtlib", "cvc3", "cvc3_smtlib", "yices", "z3"]:
    solver = [["topredicate", "-solver", solver]]
  else:
    solver = []
  if timeout > 0:
    timeout = [["topredicate", "-solvetimeout", str(timeout)]]
  else:
    timeout = []
  if validity:
    mode = [["topredicate", "-validity"]]
  else:
    mode = [["topredicate", "-sat"]]
  if filename == None:
    filename = tempfile.mkstemp(".stp", "bap_")[1]
  return config + solver + timeout + mode + [["topredicate", "-stp-out", filename], ["topredicate", "-solve"]]
