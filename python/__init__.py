#!/usr/bin/env python
#
# __init__.py
# Copyright (C) 2013 Carl Pulley <c.j.pulley@hud.ac.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details. 
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 

from bap.libbap import Program, VolatilityObject
from bap.commands import *

#---------------------------------------------------------------
# Predefined Varibles
#---------------------------------------------------------------

def x86_regs():
  return [
    (0, "R_EBP", { 'reg': 32 }),
    (1, "R_ESP", { 'reg': 32 }),
    (2, "R_ESI", { 'reg': 32 }),
    (3, "R_EDI", { 'reg': 32 }),
    (4, "R_EIP", { 'reg': 32 }),
    (5, "R_EAX", { 'reg': 32 }),
    (6, "R_EBX", { 'reg': 32 }),
    (7, "R_ECX", { 'reg': 32 }),
    (8, "R_EDX", { 'reg': 32 }),
    (9, "EFLAGS", { 'reg': 32 }),
    (10, "R_CF", { 'reg': 1 }),
    (11, "R_PF", { 'reg': 1 }),
    (12, "R_AF", { 'reg': 1 }),
    (13, "R_ZF", { 'reg': 1 }),
    (14, "R_SF", { 'reg': 1 }),
    (15, "R_OF", { 'reg': 1 }),
    (16, "R_DFLAG", { 'reg': 32 }),
    (17, "R_FS_BASE", { 'reg': 32 }),
    (18, "R_GS_BASE", { 'reg': 32 }),
    (19, "R_CS", { 'reg': 16 }),
    (20, "R_DS", { 'reg': 16 }),
    (21, "R_ES", { 'reg': 16 }),
    (22, "R_FS", { 'reg': 16 }),
    (23, "R_GS", { 'reg': 16 }),
    (24, "R_SS", { 'reg': 16 }),
    (25, "R_GDT", { 'reg': 32 }),
    (26, "R_LDT", { 'reg': 32 }),
    (27, "R_FPU_CONTROL", { 'reg': 16 }),
    (28, "R_MXCSR", { 'reg': 32 }),
    (29, "R_CC_OP", { 'reg': 32 }),
    (30, "R_CC_DEP1", { 'reg': 32 }),
    (31, "R_CC_DEP2", { 'reg': 32 }),
    (32, "R_CC_NDEP", { 'reg': 32 }),
    (33, "R_IDFLAG", { 'reg': 32 }),
    (34, "R_ACFLAG", { 'reg': 32 }),
    (35, "R_EMWARN", { 'reg': 32 }),
    (36, "R_IP_AT_SYSCALL", { 'reg': 32 }),
    (37, "R_FTOP", { 'reg': 32 }),
    (38, "R_FPROUND", { 'reg': 32 }),
    (39, "R_FC3210", { 'reg': 32 }),
    (40, "R_XMM0", { 'reg': 128 }),
    (41, "R_XMM1", { 'reg': 128 }),
    (42, "R_XMM2", { 'reg': 128 }),
    (43, "R_XMM3", { 'reg': 128 }),
    (44, "R_XMM4", { 'reg': 128 }),
    (45, "R_XMM5", { 'reg': 128 }),
    (46, "R_XMM6", { 'reg': 128 }),
    (47, "R_XMM7", { 'reg': 128 }),
    (48, "R_ST0", { 'reg': 80 }),
    (49, "R_ST1", { 'reg': 80 }),
    (50, "R_ST2", { 'reg': 80 }),
    (51, "R_ST3", { 'reg': 80 }),
    (52, "R_ST4", { 'reg': 80 }),
    (53, "R_ST5", { 'reg': 80 }),
    (54, "R_ST6", { 'reg': 80 }),
    (55, "R_ST7", { 'reg': 80 }),
  ]

def x86_mem():
  return [ (len(x86_regs()), "mem", { 'tmem': { 'reg': 32 } }) ]

def arm_regs():
  regs = [
    (0, "R0", { 'reg': 32 }),
    (1, "R1", { 'reg': 32 }),
    (2, "R2", { 'reg': 32 }),
    (3, "R3", { 'reg': 32 }),
    (4, "R4", { 'reg': 32 }),
    (5, "R5", { 'reg': 32 }),
    (6, "R6", { 'reg': 32 }),
    (7, "R7", { 'reg': 32 }),
    (8, "R8", { 'reg': 32 }),
    (9, "R9", { 'reg': 32 }),
    (10, "R10", { 'reg': 32 }),
    (11, "R11", { 'reg': 32 }),
    (12, "R12", { 'reg': 32 }),
    (13, "R13", { 'reg': 32 }),
    (14, "R14", { 'reg': 32 }),
    (15, "R15T", { 'reg': 32 }),
    (16, "CC", { 'reg': 32 }),
    (17, "CC_OP", { 'reg': 32 }),
    (18, "CC_DEP1", { 'reg': 32 }),
    (19, "CC_DEP2", { 'reg': 32 }),
    (20, "CC_NDEP", { 'reg': 32 }),
  ]
  return [ (n+len(x86_regs())+len(x86_mem()), r, t) for n, r, t in regs ]

def all_regs():
  return x86_mem() + x86_regs() + arm_regs()

def get_x86_var(reg):
  return [ (n, r, t) for n, r, t in x86_regs() if r == reg ][0]

def get_arm_var(reg):
  return [ (n, r, t) for n, r, t in arm_regs() if r == reg ][0]

def var_to_str(v):
  return v[1]+"_"+str(v[0])

#---------------------------------------------------------------
# Verification Condition Helpers
#---------------------------------------------------------------

def vclist():
  """
  A list of supported Verification Condition (VC) commands. 

  VCs with parameters (e.g. like num) are not included.
  """
  return [
    "vc.dwp",
    "vc.fwp",
    "vc.fwpuwp",
    "vc.fwplazyconc",
    "vc.fwplazyconcuwp",
    "vc.dwplet",
    "vc.dwp1",
    "vc.pwp",
    "vc.flanagansaxe",
    "vc.wp",
    "vc.uwp",
    "vc.uwpe",
    "stp.fse_bfs",
  ]

def pred_vclist():
  """
  A list of supported Verification Condition (VC) commands that only require 
  predicate logic (i.e. no quantifiers).

  VCs with parameters (e.g. like num) are not included.
  """
  return [
    "vc.dwp",
    "vc.fwp",
    "vc.fwpuwp",
    "vc.fwplazyconc",
    "vc.fwplazyconcuwp",
    "vc.dwplet",
    "vc.pwp",
    "vc.flanagansaxe",
    "vc.wp",
    "vc.uwp",
    "vc.uwpe",
    "stp.fse_bfs",
  ]
