(* example.ml                                                              *)
(* Copyright (C) 2013 Carl Pulley <c.j.pulley@hud.ac.uk>                   *)
(*                                                                         *)
(* This program is free software; you can redistribute it and/or modify    *)
(* it under the terms of the GNU General Public License as published by    *)
(* the Free Software Foundation; either version 2 of the License, or (at   *)
(* your option) any later version.                                         *)
(*                                                                         *)
(* This program is distributed in the hope that it will be useful, but     *)
(* WITHOUT ANY WARRANTY; without even the implied warranty of              *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        *)
(* General Public License for more details.                                *)
(*                                                                         *)
(* You should have received a copy of the GNU General Public License       *)
(* along with this program; if not, write to the Free Software             *)
(* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA *)

open Volatility
open Unsigned

class _AddressSpace addrspace = 
object(self)
  method read addr length = (as_Option as_PyString)(_VolatilityObject_callback addrspace [
      PyMethod( "read", PyTuple [| PyInt addr; PyInt length |], empty_dict )
    ])

  method zread addr length = as_PyString(_VolatilityObject_callback addrspace [
      PyMethod( "zread", PyTuple [| PyInt addr; PyInt length |], empty_dict )
    ])

  method is_valid_address addr = as_PyBool(_VolatilityObject_callback addrspace [
      PyMethod( "is_valid_address", PyTuple [| PyInt addr |], empty_dict )
    ])

  method get_available_pages = 
    let pages = as_PyObject(_VolatilityObject_callback addrspace [ PyMethod( "get_available_pages", empty_tuple, empty_dict ) ])
    in
      py_iter as_PyTuple pages

  method get_pdpi addr = as_PyInt(_VolatilityObject_callback addrspace [
      PyMethod( "get_pdpi", PyTuple [| PyInt addr |], empty_dict )
    ])

  method get_pgd addr pdpte = as_PyInt(_VolatilityObject_callback addrspace [
      PyMethod( "get_pgd", PyTuple [| PyInt addr; PyInt pdpte |], empty_dict )
    ])

  method get_pte addr pde = as_PyInt(_VolatilityObject_callback addrspace [
      PyMethod( "get_pte", PyTuple [| PyInt addr; PyInt pde |], empty_dict )
    ])
  
  method is_writeable addr =
    let pdpte = self#get_pdpi addr in
    let pde = self#get_pgd addr pdpte in
    let pte = self#get_pte addr pde in
      (UInt64.logand (UInt64.shift_right pte 1) UInt64.one) = UInt64.one
  
  method is_executable addr =
    let pdpte = self#get_pdpi addr in
    let pde = self#get_pgd addr pdpte in
    let pte = self#get_pte addr pde in
      (UInt64.shift_right pte 63) = UInt64.zero
end

class _EPROCESS eproc =
object(self)
  method pid = as_PyInt(_VolatilityObject_callback eproc [
      PyMethod( "__getattr__", PyTuple [| PyString "UniqueProcessId" |], empty_dict );
      PyMethod( "v", empty_tuple, empty_dict )
    ])

  method get_process_address_space = as_PyObject(_VolatilityObject_callback eproc [
      PyMethod( "get_process_address_space", empty_tuple, empty_dict )
    ])
end

let plugin_example vol_eproc =
  let eproc = new _EPROCESS(vol_eproc) in
  let addrspace = new _AddressSpace(eproc#get_process_address_space) in
  let process_page page =
    let base_addr = as_PyInt(Array.get page 0) in
      if addrspace#is_writeable base_addr || addrspace#is_executable base_addr then print_string ((Printf.sprintf "0x%08x" (UInt64.to_int base_addr)) ^ ": ");
      if addrspace#is_writeable base_addr then print_string "W";
      if addrspace#is_executable base_addr then print_string "X";
      if addrspace#is_writeable base_addr || addrspace#is_executable base_addr then print_endline ""
  in
    BatLazyList.iter process_page addrspace#get_available_pages

let _ = Callback.register "example" plugin_example
