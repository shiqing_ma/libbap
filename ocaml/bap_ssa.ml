(* bap_ssa.ml                                                              *)
(* Copyright (C) 2013 Carl Pulley <c.j.pulley@hud.ac.uk>                   *)
(*                                                                         *)
(* This program is free software; you can redistribute it and/or modify    *)
(* it under the terms of the GNU General Public License as published by    *)
(* the Free Software Foundation; either version 2 of the License, or (at   *)
(* your option) any later version.                                         *)
(*                                                                         *)
(* This program is distributed in the hope that it will be useful, but     *)
(* WITHOUT ANY WARRANTY; without even the implied warranty of              *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        *)
(* General Public License for more details.                                *)
(*                                                                         *)
(* You should have received a copy of the GNU General Public License       *)
(* along with this program; if not, write to the Free Software             *)
(* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA *)

open Volatility
open Ssa

let int_to_python = Bap_ast.int_to_python

let int64_to_python = Bap_ast.int64_to_python

let value_to_python = function
  | Int(i, t) ->
      PyDict [ (PyString "int", PyTuple [| Bap_ast.big_int_to_python i; Bap_ast.typ_to_python t |]) ]
  | Var(v) ->
      PyDict [ (PyString "var", Bap_ast.var_to_python v) ]
  | Lab(l) ->
      PyDict [ (PyString "lab", PyString l) ]

let exp_to_python = function
  | Load(v1, v2, v3, t) ->
      PyDict [ (PyString "load", PyTuple [| value_to_python v1; value_to_python v2; value_to_python v3; Bap_ast.typ_to_python t |]) ]
  | Store(v1, v2, v3, v4, t) ->
      PyDict [ (PyString "store", PyTuple [| value_to_python v1; value_to_python v2; value_to_python v3; value_to_python v4; Bap_ast.typ_to_python t |]) ]
  | Ite(vB, vT, vF) ->
      PyDict [ (PyString "ite", PyTuple [| value_to_python vB; value_to_python vT; value_to_python vF |]) ]
  | Extract(i1, i2, v) ->
      PyDict [ (PyString "extract", PyTuple [| Bap_ast.big_int_to_python i1; Bap_ast.big_int_to_python i2; value_to_python v |]) ]
  | Concat(v1, v2) ->
      PyDict [ (PyString "concat", PyTuple [| value_to_python v1; value_to_python v2 |]) ]
  | BinOp(op, v1, v2) ->
      PyDict [ (PyString "binop", PyTuple [| Bap_ast.binop_to_python op; value_to_python v1; value_to_python v2 |]) ]
  | UnOp(op, v) ->
      PyDict [ (PyString "unop", PyTuple [| Bap_ast.unop_to_python op; value_to_python v |]) ]
  | Val(v) ->
      PyDict [ (PyString "val", value_to_python v) ]
  | Cast(ct, t, v) ->
      PyDict [ (PyString "cast", PyTuple [| Bap_ast.cast_to_python ct; Bap_ast.typ_to_python t; value_to_python v |]) ]
  | Unknown(s, t) ->
      PyDict [ (PyString "unknown", PyTuple [| PyString s; Bap_ast.typ_to_python t |]) ]
  | Phi(vs) ->
      PyDict [ (PyString "phi", Bap_ast.vars_to_python vs) ]

let stmt_to_python = function
  | Move(v, e, a) ->
      PyDict [ (PyString "move", PyTuple [| exp_to_python e; Bap_ast.attrs_to_python a |]) ]
  | Jmp(v, a) ->
      PyDict [ (PyString "jmp", Bap_ast.attrs_to_python a) ]
  | CJmp(vB, vT, vF, a) ->
      PyDict [ (PyString "cjmp", PyTuple [| value_to_python vB; value_to_python vT; value_to_python vF; Bap_ast.attrs_to_python a |]) ]
  | Label(l, a) ->
      PyDict [ (PyString "label", PyTuple [| Bap_ast.label_to_python l; Bap_ast.attrs_to_python a |]) ]
  | Halt(v, a) ->
      PyDict [ (PyString "halt", PyTuple [| value_to_python v; Bap_ast.attrs_to_python a |]) ]
  | Assert(v, a) ->
      PyDict [ (PyString "assert", PyTuple [| value_to_python v; Bap_ast.attrs_to_python a |])]
  | Assume(v, a) ->
      PyDict [ (PyString "assume", PyTuple [| value_to_python v; Bap_ast.attrs_to_python a |])]
  | Comment(c, a) ->
      PyDict [ (PyString "comment", PyTuple [| PyString c; Bap_ast.attrs_to_python a |])]

let ast_to_python stmt_list = PyList(List.map stmt_to_python stmt_list)

let node_to_python ssa node py_nodes =
  let py_stmts = ast_to_python(Cfg.SSA.get_stmts ssa node) in
  let py_label = Bap_cfg.bbid_to_python(Cfg.SSA.G.V.label node) in
  let py_hash = int_to_python(Cfg.SSA.G.V.hash node) in
  let py_node = PyDict [ (PyString "hash", py_hash); (PyString "label", py_label); (PyString "stmts", py_stmts) ] in
    py_node :: py_nodes

let edge_to_python edge py_edges =
  let label = Cfg.SSA.G.E.label edge in
  let py_source = int_to_python(Cfg.SSA.G.V.hash(Cfg.SSA.G.E.src edge)) in
  let py_target = int_to_python(Cfg.SSA.G.V.hash(Cfg.SSA.G.E.dst edge)) in
  let py_edge =
    match label with
    | Some b ->
        PyDict [ (PyString "source", py_source); (PyString "target", py_target); (PyString "label", PyBool b) ]
    | None ->
        PyDict [ (PyString "source", py_source); (PyString "target", py_target) ]
  in
    py_edge :: py_edges

let to_python ssa =
  let py_nodes = Cfg.SSA.G.fold_vertex (node_to_python ssa) ssa [] in
  let py_edges = Cfg.SSA.G.fold_edges_e edge_to_python ssa [] in
    PyDict [ (PyString "nodes", PyList py_nodes); (PyString "edges", PyList py_edges) ]
