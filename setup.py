#!/usr/bin/env python

from distutils.command.build import build as DistutilsBuild
from distutils.command.check import check as DistutilsCheck
import distutils.command.build_clib as BuildCLib
from distutils.core import setup
from distutils.extension import Extension
from distutils.errors import CompileError
from distutils.log import info, error, fatal, warn
import hashlib
import os
import shutil
import subprocess
import sys
import tarfile
from urllib import FancyURLopener

BAPVERSION = "0.7"
BAPSHA1 = "9ac448d2638b319801608323a251969a6ded2cff"

BAP = "bap-{0}".format(BAPVERSION)
BAPTAR = "{0}.tar.gz".format(BAP)
BAPURL = "http://bap.ece.cmu.edu/download/{0}".format(BAPTAR)
BAPPATCH = "{0}.patch".format(BAP)
BUILDTEMP = "build/temp.linux-x86_64-2.7" # FIXME: needs to be generically defined
OCAMLLIB = subprocess.check_output(["ocamlc", "-where"]).strip()

INCLUDES = [
  OCAMLLIB,
  "{0}/{1}/ocamlgraph-1.8".format(BUILDTEMP, BAP),
  "{0}/{1}/objsize-0.16".format(BUILDTEMP, BAP),
  "{0}/{1}/ocaml".format(BUILDTEMP, BAP),
  "{0}/{1}/libtracewrap/libtrace/src/ocaml".format(BUILDTEMP, BAP),
  "{0}/ocaml".format(BUILDTEMP),
]

LIBS = [
  OCAMLLIB,
  "{0}/../ctypes".format(OCAMLLIB),
  "{0}/../llvm".format(OCAMLLIB),
  "{0}/..".format(OCAMLLIB),
  "{0}/../camlidl".format(OCAMLLIB),
  "{0}/../piqi".format(OCAMLLIB),
  "{0}/../zarith".format(OCAMLLIB),
  "{0}/{1}/ocaml".format(BUILDTEMP, BAP),
  "{0}/{1}/ocamlgraph-1.8".format(BUILDTEMP, BAP),
  "{0}/{1}/objsize-0.16".format(BUILDTEMP, BAP),
  "{0}/{1}/libasmir/src".format(BUILDTEMP, BAP),
  "{0}/{1}/VEX".format(BUILDTEMP, BAP),
  "{0}/ocaml".format(BUILDTEMP),
]

SHAREDLIBS = [
  "camlrun_shared", 
  "unix", 
  "nums", 
  "camlidl", 
  "bap_stubs", 
  "zarith", 
  "objsize", 
  "bigarray", 
  "piqilib_stubs", 
  "camlstr", 
  "asmir", 
  "bfd", 
  "c", 
  "stdc++", 
  "vex", 
  "gmp",
  "opcodes",
  "threads",
  "python2.7",
  "ctypes_stubs",
  "ctypes-foreign_stubs",
  "ffi",
  "LLVMCore",
  "LLVM-3.2svn",
  "llvm_executionengine",
  "LLVMExecutionEngine",
  "llvm_bitwriter",
  "llvm_target",
  "llvm_scalar_opts",
  "llvm_analysis",
  "llvm",
]

class LibbapCheck(DistutilsCheck):
  def run(self):
    if os.popen("which ocaml").read() == "":
      fatal("In order to use this package, ocaml needs to be installed")
    if os.popen("which patch").read() == "":
      fatal("In order to use this package, patch needs to be installed")
    if os.popen("which xdot").read() == "":
      fatal("In order to use this package, xdot needs to be installed")

class LibbapBuild(DistutilsBuild):
  def run(self):
    # download BAP tar ball if its not already present
    if not os.path.exists(BAPTAR):
      self.lastprog = None
      def progress(blocks, blocksz, totalsz):
        if self.lastprog == None:
          info("Connected: downloading BAP tar ball...")
        percent = int((100*(blocks*blocksz)/float(totalsz)))
        if self.lastprog != percent and percent % 5 == 0: 
          sys.stdout.write("{0}%..".format(percent))
          sys.stdout.flush()
        if percent == 100:
          sys.stdout.write("\n")
          sys.stdout.flush()
        self.lastprog = percent
    
      if not os.path.exists("{0}/{1}".format(self.build_temp, BAPTAR)):
        if not os.path.exists(self.build_temp):
          os.makedirs(self.build_temp)
        # DEBUG:
        shutil.copyfile("/home/bagpuss/{0}".format(BAPTAR), "{0}/{1}".format(self.build_temp, BAPTAR))
        #FancyURLopener().retrieve(BAPURL, "{0}/{1}".format(self.build_temp, BAPTAR), reporthook=progress)
    
    # check downloaded SHA1 against BAPSHA1
    info("Checking SHA1 hash of BAP tar ball..")
    sha1 = hashlib.sha1()
    f = open("{0}/{1}".format(self.build_temp, BAPTAR), 'rb')
    try:
      sha1.update(f.read())
    finally:
      f.close()
    if sha1.hexdigest() != BAPSHA1:
      fatal("BAP download failed: SHA1 hash is not correct")
      raise CompileError()
    
    if not os.path.exists("{0}/{1}".format(self.build_temp, BAP)):
      # unpack pristine BAP tarball
      info("Unpacking BAP tar ball..")
      tarball = tarfile.open("{0}/{1}".format(self.build_temp, BAPTAR))
      tarball.extractall(self.build_temp)
      tarball.close()

    # if available, apply patch to unpacked (pristine) BAP tar ball
    if os.path.exists(BAPPATCH) and not os.path.exists("{0}/{1}/.patched".format(self.build_temp, BAP)):
      info("Applying patch to BAP tar ball..")
      if subprocess.call("patch -p1 -d {0}/{1} < {2}".format(self.build_temp, BAP, BAPPATCH), shell=True) != 0:
        raise CompileError("Failed to patch BAP tar ball")
      os.system("touch {0}/{1}/.patched".format(self.build_temp, BAP))

    # build patched BAP tar ball
    if not os.path.exists("{0}/{1}/Makefile".format(self.build_temp, BAP)):
      if subprocess.call("cd {0}/{1}; ./configure --with-llvm".format(self.build_temp, BAP), shell=True) != 0:
        raise CompileError("Failed to configure BAP")
    if not os.path.exists("{0}/{1}/ocaml/bap.a".format(self.build_temp, BAP)):
      info("Building BAP..")
      if subprocess.call("cd {0}/{1}; make".format(self.build_temp, BAP), shell=True) != 0:
        raise CompileError("Failed to build BAP")

    # build other packaged OCaml source files
    if not os.path.exists("{0}/ocaml".format(self.build_temp)):
      shutil.copytree("ocaml", "{0}/ocaml".format(self.build_temp))
    info("Building libbap OCaml files..")
    if subprocess.call("cd {0}/ocaml; make".format(self.build_temp), shell=True) != 0:
      raise CompileError("Failed to build libbap OCaml sources")

    DistutilsBuild.run(self)

# The libbap C extension module
extension_mod = Extension("bap.libbap", 
  ["src/libbap.c"], 
  extra_objects=["{0}/ocaml/bap.o".format(BUILDTEMP), "{0}/{1}/ocaml/libbfd_stubs.o".format(BUILDTEMP, BAP)],
  extra_link_args=["-rdynamic", "-g"],
  runtime_library_dirs=["{0}/ocaml".format(BUILDTEMP), OCAMLLIB, "{0}/..".format(OCAMLLIB), "{0}/../llvm".format(OCAMLLIB)], 
  include_dirs=INCLUDES, 
  library_dirs=LIBS, 
  libraries=SHAREDLIBS,
)

setup(
  name = "bap", 
  version='1.0',
  description='Python/Volatility wrapper for the Binary Analysis Platform (BAP)',
  author='Carl Pulley',
  author_email='c.j.pulley@hud.ac.uk',
  url='https://bitbucket.org/carlpulley/libbap/',
  classifiers=[
    'License :: OSI Approved :: GNU General Public License (GPL)',
  ],
  ext_modules=[extension_mod],
  py_modules=['bap.commands.analysis', 'bap.commands.stp', 'bap.commands.vc', 'bap.commands.codegen'],
  packages=['bap', 'bap.commands'],
  package_dir={ 'bap': 'python' },
  cmdclass={ 'check': LibbapCheck, 'build': LibbapBuild },
)
